#!/bin/sh
#It's reported that connection to some DNS server (or any other server on internet) is intermittent.
#This script is intended for periodically check the traceroute and log the result to file.
check_trace() {
        out_file="/root/trace.out"
        trace_src_ip=$(ip -f inet -o addr | grep bond0 | awk '{print $4}' | cut -d/ -f1)
        dst_ip=$1
        
        trace_cmd="traceroute -s $trace_src_ip $dst_ip -U -p53"
        
        echo -e "################################\n`date`\nHOSTNAME: $HOSTNAME\n################################" >> $out_file
        echo $trace_cmd >> $out_file && traceroute -s $trace_src_ip $dst_ip -U -p53 >> $out_file
        echo -e '\n' >> $out_file
}

#traceroute to google public dns
check_trace 8.8.8.8

#traceroute to A root server
check_trace 198.41.0.4

if [[ $(date '+%H' | sed 's/^0//') -eq 1 && ! -e /root/traceOut/trace.out.$(date '+%d%b') ]];then 
	mv trace.out /root/traceOut/trace.out.$(date '+%d%b')
	> trace.out
fi

