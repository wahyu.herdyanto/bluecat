#/bin/bash
#this script is for Bluecat DNS server
#######################################################
## Created by : wahyu.herdyanto@dimensiondata.com #####
## use this script for searching on which page the ####
## domain you want to take out from RPZ            ####
## The string you input must be exactly	           ####
## the name you're looking for.                    ####
#######################################################
#use this command with this format: ./search_rpz.sh [domain_name]
if [[ $1 == '' ]];then
	echo "usage: ./search_rpz.sh [domain_name]"
	exit 1
fi

echo "searching now..."
page_size=50
rpz_dir='/replicated/jail/named/var/dns-config/dbs/'
rpz_file=$(ls $rpz_dir | egrep 'redirected\-[0-9]+\.db')
rpz_full_path="$rpz_dir$rpz_file"

line=$(sort -r $rpz_full_path | sed -e 1,4d | awk '{print $1}' | grep -n -w ^$1$ | cut -d : -f 1)
if [[ $line != '' ]];then
	if [[ $((line / page_size)) == 0 ]];then
		page=1;
	elif [[ $(( line % page_size )) == 0 ]];then
		page=$(( line / page_size ));
	else
		page=$(( line / page_size + 1 ));
	fi
	
else
	echo "domain does NOT exist"
	exit 1
fi

echo $page

exit 0