#!/usr/local/bin/python3.5
#mailto: wahyu.herdyanto@dimensiondata.com
#this script is to check DNS response consistency of certain DNS server againts specific domains.
#the query will be run periodically and result of query will be logged 

import os
import subprocess
import datetime

logfile = "/var/log/dig.log"
logfile1gz = "/var/log/dig.log.1.gz"

x = current_datetime = datetime.datetime.now()
try:
	y = last_modtime = datetime.datetime.fromtimestamp(os.path.getmtime(logfile1gz))
except FileNotFoundError:
	y = last_modtime = datetime.datetime.fromtimestamp(0)

#print("y.year : %d | y.month : %d | y.day : %d | y.hour : %d | y.minute : %d" %(y.year, y.month, y.day, y.hour, y.minute))
#print("x.year : %d | x.month : %d | x.day : %d | x.hour : %d | x.minute : %d" %(x.year, x.month, x.day, x.hour, x.minute))
#print( datetime.datetime(y.year, y.month, y.day, y.hour, y.minute) < datetime.datetime(x.year, x.month, x.day, x.hour, x.minute) )

if (x.hour == 4) and ( datetime.date(y.year, y.month, y.day) < datetime.date(x.year, x.month, x.day) ):
	i = 10
	while i >= 1:
		if i == 10:
			subprocess.Popen('rm' + ' -f ' + logfile + '.' + str(i) + '.gz', shell=True, stderr=subprocess.PIPE)
		else:
			j = i + 1
			subprocess.Popen('mv ' + logfile + '.' + str(i) + '.gz' + ' ' + logfile + '.' + str(j) + '.gz', shell=True, stderr=subprocess.PIPE)
		i -= 1
	subprocess.Popen('gzip -S .1.gz ' + logfile, shell=True, stderr=subprocess.PIPE)
	subprocess.Popen('touch ' + logfile1gz, shell=True, stderr=subprocess.PIPE)

def diglog(hostname):
	p = subprocess.Popen('dig ' + hostname, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	f = open(logfile, 'a')
	timestamp = "{:%Y-%b-%d %H:%M:%S}".format(datetime.datetime.now())

	hashstr_len = len(timestamp) + 4
	hashstr = '#' * hashstr_len

	f.write('\n\n' + hashstr + '\n# ' + timestamp + ' #\n' + hashstr + '\n')

	for line in p.stdout:
		f.write(line.decode('utf-8'))
	f.close()

def main():
	diglog("data-api-prod.opensignal.com")
	diglog("opensignalspeedtest.mdc.akamaized.net")
	
if __name__ == "__main__":
	main()
