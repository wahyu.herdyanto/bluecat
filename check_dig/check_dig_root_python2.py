#!/usr/bin/python
#mailto: wahyu.herdyanto@dimensiondata.com
#this script is for checking query to root server with specific domain com.
#This checking is necessary when there's case upstream provider block query to certain root server. (False Alarm)

import os
import re
from subprocess import PIPE, Popen, call
from datetime import datetime, date

ls_root = ('198.41.0.4', \
		'192.228.79.201', \
		'192.33.4.12', \
		'199.7.91.13', \
		'192.203.230.10', \
		'192.5.5.241', \
		'192.112.36.4', \
		'198.97.190.53', \
		'192.36.148.17', \
		'192.58.128.30', \
		'193.0.14.129', \
		'199.7.83.42', \
		'202.12.27.33')
		

logfile = "/var/log/dig/digroot.log"
logfile1gz = "/var/log/dig/digroot.log.1.gz"

logdir = os.path.dirname(logfile)
if not (os.path.isdir(logdir)):
	try:
		os.makedirs(logdir)
	except:
		print "dir already created"

cur_datetime = datetime.now()
try:
	lastmod_datetime = datetime.fromtimestamp(os.path.getmtime(logfile1gz))
except OSError:
	lastmod_datetime = datetime.fromtimestamp(0)

#print("y.year : %d | y.month : %d | y.day : %d | y.hour : %d | y.minute : %d" %(y.year, y.month, y.day, y.hour, y.minute))
#print("x.year : %d | x.month : %d | x.day : %d | x.hour : %d | x.minute : %d" %(x.year, x.month, x.day, x.hour, x.minute))
#print( datetime(y.year, y.month, y.day, y.hour, y.minute) < datetime(x.year, x.month, x.day, x.hour, x.minute) )

def logrotate(x, y, logfile, logfile1gz):
	if (x.hour == 4) and ( date(y.year, y.month, y.day) < date(x.year, x.month, x.day) ):
		i = 10
		while i >= 1:
			if i == 10:
				Popen('/bin/rm' + ' -f ' + logfile + '.' + str(i) + '.gz', shell=True, stderr=PIPE)
			else:
				j = i + 1
				Popen('/bin/mv ' + logfile + '.' + str(i) + '.gz' + ' ' + logfile + '.' + str(j) + '.gz', shell=True, stderr=PIPE)
			i -= 1
		call('/bin/gzip -S .1.gz ' + logfile, shell=True, stderr=PIPE)
		call('/bin/touch ' + logfile1gz, shell=True, stderr=PIPE)
	return 1

def diglog(server, domain):
	timeout = 15
	timestamp = "{:%Y-%b-%d %H:%M:%S}".format(datetime.now())
	p = Popen('/usr/local/bin/dig @' + server + ' ' + domain, shell=True, stdout=PIPE, stderr=PIPE)
	stdout = p.communicate()[0].split('\n')
	timestamp = "{:%Y-%b-%d %H:%M:%S}".format(datetime.now())
	qrytime_root = ''
	status_root = ''
	with open(logfile, 'a') as f:
		for line in stdout:
			line = line.lower()
			if re.search('query time: ', line):
				qrytime_root = line.split('query time: ')[1].rstrip(' msec')
				
			elif re.search('status: ', line):
				status_root = line.split('status: ')[1].split()[0].rstrip(',')
				
			elif re.search("no servers could be reached", line):
				status_root = 'timeout'
				qrytime_root = str(timeout * 1000)
		f.write(timestamp + ' query to root ' + server + ', domain queried = com., status = ' + status_root + ', query_time = ' + qrytime_root + ' ms\n')
			
def main():
	if logrotate(cur_datetime, lastmod_datetime, logfile, logfile1gz):
		for root in ls_root:
			diglog(root, 'com.')
		
if __name__ == "__main__":
	main()
