#!/bin/sh
output="/tmp/output.txt"
output_problem="/tmp/output_problem.txt"
datetime=$(date)

function check_nf_conntrack () {
	file="/proc/net/nf_conntrack"
	if [ -e $file ];then
		nf_conntrack_lines=$(wc -l $file | awk '{print $1}')
		if [[ $nf_conntrack_lines -eq 0 ]];then
			status="OK"
			echo "nf_conntrack_status : OK"
                                
		else
			echo "nf_conntrack_status : NOK"
			return 1
		fi
	else
		echo "check_nfconntrack failed, $file does not exist"
		return 1
	fi
}

function check_cpu () {
        idle_cpu=$(top -n1 | grep Cpu | awk '{print $8}' | cut -d . -f1)
        if [[ $idle_cpu =~ ^[0-9]+$ ]] && cpu_usage=$((100 - $idle_cpu));then
				#echo "cpu idle : $idle_cpu"
				#echo "cpu usage : $cpu_usage%"
				echo "$cpu_usage"
        else
				echo "idle_cpu is NaN"
				return 1
        fi
}

function check_memory () {
        used_mem=$(free -m | grep -e \-\/\+ | awk '{print $3}')
        total_mem=$(free -m | grep "Mem:" | awk '{print $2}')
        if [[ $used_mem =~ ^[0-9]+$ ]] && [[ $total_mem =~ ^[0-9]+$ ]];then
                #echo "used_mem : $used_mem"
                #echo "total_mem : $total_mem"
                mem_usage=$(($used_mem * 100 / $total_mem))
                #echo "memory usage : $mem_usage%"
				echo "$mem_usage"
        else
                echo "used_mem and/or total_mem is NaN"
                return 1
        fi
}

function check_recursive () {
        recur_clients=$(rndc status | grep recursive | awk '{print $3}' | cut -d / -f1)
        max_recur_clients=$(rndc status | grep recursive | awk '{print $3}' | cut -d / -f3)

        if [[ $recur_clients =~ ^[0-9]+$ ]] && [[ $max_recur_clients =~ ^[0-9]+$ ]];then
                soft_limit=$(($max_recur_clients - 100))
                if [ $recur_clients -gt $soft_limit ];then
                        echo "recursive client : soft-limit exceeded"
                        return 1
                else
                        #echo "recursive clients : $recur_clients"
						echo "$recur_clients"
                fi
        fi
}

function check_tcp () {
        tcp_clients=$(rndc status | grep tcp | awk '{print $3}' | cut -d / -f1)
        max_tcp_clients=$(rndc status | grep tcp | awk '{print $3}' | cut -d / -f2)
        soft_limit=10000
        if [[ $tcp_clients =~ ^[0-9]+$ ]] && [[ $max_tcp_clients =~ ^[0-9]+$ ]];then
                if [ $tcp_clients -gt $soft_limit ];then
                        echo "tcp client soft-limit exceeded"
                        return 1
                else
                        echo "tcp clients : $tcp_clients"
                fi
        fi
}

function check_root_tld () {
                ls_root=(a.root-servers.net. \
                b.root-servers.net. \
                c.root-servers.net. \
                d.root-servers.net. \
                e.root-servers.net. \
                f.root-servers.net. \
                g.root-servers.net. \
                h.root-servers.net. \
                i.root-servers.net. \
                j.root-servers.net. \
                k.root-servers.net. \
                l.root-servers.net. \
                m.root-servers.net.)

        ls_tld=(a.gtld-servers.net. \
                b.gtld-servers.net. \
                c.gtld-servers.net. \
                d.gtld-servers.net. \
                e.gtld-servers.net. \
                f.gtld-servers.net. \
                g.gtld-servers.net. \
                h.gtld-servers.net. \
                i.gtld-servers.net. \
                j.gtld-servers.net. \
                k.gtld-servers.net. \
                l.gtld-servers.net. \
                m.gtld-servers.net.)

        isOkay_root=0
        isOkay_tld=0
        prob_root=()
        prob_tld=()
        i=0
        while [ $i -lt 13 ];do
                root_status=$(dig @${ls_root[$i]} com.|grep status |awk '{print $6}'| cut -c 1-7)
                tld_status=$(dig @${ls_tld[$i]} com.|grep status |awk '{print $6}'| cut -c 1-7)
                if [ $root_status != "NOERROR" ];then
                        prob_root[$i]=${ls_root[$i]}
                        isOkay_root=1
                elif [ $tld_status != "NOERROR" ];then
                        prob_tld[$i]=${ls_tld[$i]}
                        isOkay_tld=1
                fi
                ((i++))
        done
        if [[ $isOkay_root -eq 0 ]] && [[ $isOkay_tld -eq 0 ]];then
                echo "root_tld_servers : OK"
        else
                echo "cannot resolve domain using following root/TLD servers:\
                        ${prob_root[@]} \
                        ${prob_tld[@]}"
                return 1
        fi
}

function check_log () {
        err_count=$(cat /var/log/syslog | grep -i "$1" | wc -l)
        if [ $err_count -gt 50 ];then
                echo "many \"$1\" found in the log"
                                return 1
        else
                echo "error \"$1\" found : $err_count"
        fi
}

function genOutput () {
                echo $datetime >> $1
                check_nf_conntrack >> $1 && conntrack_status="OK" || conntrack_status="NOK"
                check_cpu >> $1 && cpu_status="OK" || cpu_status="NOK"
                check_memory >> $1 && memory_status="OK" || memory_status="NOK"
                check_recursive >> $1 && recur_status="OK" || recur_status="NOK"
                check_tcp >> $1 && tcp_status="OK" || tcp_status="NOK"
                check_root_tld >> $1 && root_tld_status="OK" || root_tld_status="NOK"
                check_log "error sending response: unset" >> $1 && log_unset_status="OK" || log_unset_status="NOK"
                check_log "maximum number of FD events (64) received" >> $1 && log_fd_status="OK" || log_fd_status="NOK"
                echo $'\n' >> $1
}

genOutput $output && [[ $conntrack_status == "OK" && $cpu_status == "OK" && $memory_status == "OK" && $recur_status == "OK" && $tcp_status == "OK" \
&& $root_tld_status == "OK" && $log_unset_status == "OK" && $log_fd_status == "OK" ]] \
|| ( $(> $output_problem) && genOutput $output_problem )


##just for checking
#echo "conntrack_status : $conntrack_status"
#[[ $conntrack_status == "OK" && $cpu_status == "OK" && $memory_status == "OK" && $recur_status == "OK" && $tcp_status == "OK" \
#&& $root_tld_status == "OK" && $log_unset_status == "OK" && $log_fd_status == "OK" ]] && health_status="OK" || health_status="NOK"
#echo "health status: $health_status"
