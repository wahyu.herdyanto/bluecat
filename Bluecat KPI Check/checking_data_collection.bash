#!/bin/bash
#this script check splunk's data collection, whether or not there's missed on certain time
#the checking result in OK state when each hour has 4 times collection, as below example for hour 0,1,2
#0 => 0 15 30 45
#1 => 0 15 30 45
#2 => 0 15 30 45

#sample creating array's name from variable
#x=25;eval "arr_$x=(1 2 3)"

#sample calling value of array's name using variable ==> indirect variable
#x=25;var="arr_$x[@]";echo ${!var}
ARGV=($@)
ARGC=$#
site=${ARGV[0]}
date=${ARGV[1]}

if [[ $ARGC -ne 2 || ${ARGV[0]} == '-h' || ${ARGV[1]} == '--help' ]];then
	echo "usage: ./script.sh site date"
	echo "Note: date format must be like 2017-Feb-15"
else
	list_time=$(awk -F ';' '{print $1}' bluecat_output.txt  | egrep "$site|$date" | cut -d ' ' -f2| sort -u)
	hour=0
	minute=0
	array=()

	for x in {0..23};do
		eval "arr_$x=()"
	done

	for t in $list_time;do
		h=$(echo "$t" | cut -d: -f1 | sed 's/^0//')
		m=$(echo "$t" | cut -d: -f2 | sed 's/^0//')
		var1="arr_$h[@]"
		arrTmp1=(${!var1})

		i=0
		while [[ $i -le 4 ]];do
			#echo $i
			if [[ ${arrTmp1[$i]} == '' ]];then
				eval "arr_$h[$i]=$m"
				break
			else
				((i++))
			fi
		done
	done

	for x in {0..23};do
		var="arr_$x[@]"
		arrTmp=(${!var})
		echo "$x => ${arrTmp[@]}"
	done
fi

#for i in {3..6};do ./test.sh tbs 2017-Feb-15 | awk -v myvar="$i" '{print $myvar}' |sed '/^$/d'|wc -l;done
