#!/usr/bin/python
#This script is expected to be run from external machine (not from individual Bluecat machine).
#To acquire necessary information of certain metrics, either SSH access to Bluecat machine or snmpaccess (or snmpwalk) is necessary.

import sys
import time
import threading
import re
import paramiko
import socket
import Queue
from paramiko import ssh_exception
from subprocess import Popen, PIPE
from datetime import datetime

tup_root = ('198.41.0.4', \
		'192.228.79.201', \
		'192.33.4.12', \
		'199.7.91.13', \
		'192.203.230.10', \
		'192.5.5.241', \
		'192.112.36.4', \
		'198.97.190.53', \
		'192.36.148.17', \
		'192.58.128.30', \
		'193.0.14.129', \
		'199.7.83.42', \
		'202.12.27.33')
		
dclient = {}
dthread = {}
dretval = {}
dretval_get = {}
dchan = {}
lthread = []
lthread_jpr = []
ssh_connected = {}

error_logfile = "/var/log/bluecat_kpi_error.log"
hosts = ("10.x.x.1", "10.x.x.2", "10.x.x.3")

#ChangeMe
username = "$USER"
password = "$PASSWORD"

chan_timeout = 15
for host in hosts:
	ssh_connected[host] = 1

dd_walk = "snmpwalk -v2c -c dnsgiro "

timestamp = "{0:%Y-%b-%d %H:%M:%S}".format(datetime.now())

dip_hostname = { '10.x.x.1' : 'dns1.example.com', \
'10.x.x.2' : 'dns2.example.com', \
'10.x.x.3' : 'dns3.example.com' \
}

def write_to_file(host, funcname, error):
	with open(error_logfile,'a') as f:		
		f.write(timestamp + " (" + host + ") [" + funcname + "] " + error + '\n')
	
def check_qps(host, retval, username, password):
	f_name = 'check_qps'
	try:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.13315.3.1.2.2.2.1", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0].split('\n')
		for line in stdout:
			if re.search('.3.1.2.2.2.1.1', line):
				success = line.split('Counter64: ')[1].rstrip('\n')
			elif re.search('.3.1.2.2.2.1.2', line):
				referral = line.split('Counter64: ')[1].rstrip('\n')
			elif re.search('.3.1.2.2.2.1.3', line):
				nxrrset = line.split('Counter64: ')[1].rstrip('\n')
			elif re.search('.3.1.2.2.2.1.4', line):
				nxdomain = line.split('Counter64: ')[1].rstrip('\n')
			elif re.search('.3.1.2.2.2.1.6', line):
				failure = line.split('Counter64: ')[1].rstrip('\n')
		
		ls_qps = [success, referral, nxrrset, nxdomain, failure]
		
	except (UnboundLocalError, IndexError) as e:
		write_to_file(dip_hostname[host], f_name, str(e))
		ls_qps = ['', '', '', '', '']
	except:
		write_to_file(dip_hostname[host], f_name, "something wrong happened")
		ls_qps = ['', '', '', '', '']
	
	str_qps = ""
	for qps in ls_qps:
		str_qps += qps + ';NULL;'
	
	retval.put((str_qps,))
	
def check_cpu(host, retval, username, password):
	f_name = 'check_cpu'
	try:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.2021.11.11", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0].split('\n')
		cpu_usage = 100 - int(stdout[0].split('INTEGER: ')[1].rstrip())
		retval.put((str(cpu_usage) + '%', '80%'))
	except (UnboundLocalError, IndexError) as e:
		write_to_file(dip_hostname[host], f_name, str(e))
		retval.put(('',''))
	except:
		write_to_file(dip_hostname[host], f_name, "something wrong happened")
		retval.put(('',''))
		
def check_memory(host, retval, username, password):
	f_name = 'check_memory'
	try:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.2021.4 -On", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0].split('\n')
		for line in stdout:
			if re.search('2021.4.6', line):
				free = line.split('INTEGER: ')[1].rstrip(' kB')
			elif re.search('2021.4.14', line):
				buffered = line.split('INTEGER: ')[1].rstrip(' kB')
			elif re.search('2021.4.15', line):
				cached = line.split('INTEGER: ')[1].rstrip(' kB')
			elif re.search('2021.4.5', line):
				total = line.split('INTEGER: ')[1].rstrip(' kB')
				
		mem_usage = (int(total) - int(free) - int(buffered) - int(cached)) * 100 / int(total) 
		retval.put((str(mem_usage) + '%', '90%'))
		
	except (UnboundLocalError, IndexError) as e:
		write_to_file(dip_hostname[host], f_name, str(e))
		retval.put(('',''))
	except:
		write_to_file(dip_hostname[host], f_name, "something wrong happened")
		retval.put(('',''))

def check_recursion(host, retval, username, password):
	f_name = 'check_recursion'
	try:
		p = Popen(dd_walk + host + " .1.3.6.1.4.1.13315.3.1.2.2.2.1.5", shell=True, stdout=PIPE, stderr=PIPE)
		stdout = p.communicate()[0].split('\n')
		recursion = stdout[0].split('Counter64: ')[1].rstrip()
		rec_max_value = '3000'
		retval.put((recursion, rec_max_value))
	
	except (UnboundLocalError, IndexError) as e:
		write_to_file(dip_hostname[host], f_name, str(e))
		retval.put(('',''))
	except:
		write_to_file(dip_hostname[host], f_name, "something wrong happened")
		retval.put(('',''))
		
def check_tcp_query(host, retval, username, password):
	f_name = 'check_tcp_query'
	if ssh_connected[host]:
		try:
			hostfunc = host + f_name
			dchan[hostfunc] = dclient[host].get_transport().open_session()
			dchan[hostfunc].exec_command("rndc status | grep tcp")
			dchan[hostfunc].settimeout(chan_timeout)
			stdout = dchan[hostfunc].recv(256).strip('\n')
			tcp_query = stdout.split('tcp clients: ')[1].split('/')[0]
			tcp_max_value = stdout.split('tcp clients: ')[1].split('/')[1]
			retval.put((tcp_query, tcp_max_value))
			
		except (UnboundLocalError, IndexError, ssh_exception.SSHException) as e:
			write_to_file(dip_hostname[host], f_name, str(e))
			retval.put(('',''))
		except:
			write_to_file(dip_hostname[host], f_name, "something wrong happened")
			retval.put(('',''))
		
def check_root(host, retval, username, password):
	f_name = 'check_root'
	if ssh_connected[host]:
		try:
			dqrystatus = {}
			dqrytime = {}
			hostfunc = host + f_name
			str_egrep_root = ""
			ls_root = list(tup_root)
			for root in tup_root:
				str_egrep_root += root + '|'
			str_egrep_root = '"' + str_egrep_root.rstrip('|') + '"'
			
			try:
				dchan[hostfunc] = dclient[host].get_transport().open_session()
				dchan[hostfunc].exec_command('tail -n 13 /var/log/dig/digroot.log| egrep ' + str_egrep_root + ' | sort -u -t" " -k6,6| sort -t" " -k2,2')
				dchan[hostfunc].settimeout(chan_timeout)
				stdout = dchan[hostfunc].recv(2048)
				
				for line in stdout.rstrip('\n').split('\n'):
					ls_line = line.split(' ')
					root = ls_line[5].rstrip(',')
					status = ls_line[12].rstrip(',')
					qrytime = ls_line[15]
					
					ls_root.remove(root)
					dqrystatus[root] = status
					dqrytime[root] = qrytime
				
				if len(ls_root) != 0:
					for root in ls_root:
						dqrystatus[root] = ''
						dqrytime[root] = '0'

			except socket.timeout:
				for root in tup_root:
					dqrystatus[root] = 'timeout'
					dqrytime[root] = str(chan_timeout * 1000)
				
			#calculate and produce status success rate
			str_qrystatus = ''
			for root in tup_root:
				str_qrystatus += dqrystatus[root] + ';NULL;'
			
			#calculate and produce query time per root
			str_qrytime = ''
			for root in tup_root:
				str_qrytime += dqrytime[root] + ';' + str(chan_timeout * 1000) + ';'
			
			retval.put((str_qrystatus, str_qrytime))
			
		except (UnboundLocalError, IndexError) as e:
			write_to_file(dip_hostname[host], f_name, str(e))
			retval.put(('',''))
		except:
			write_to_file(dip_hostname[host], f_name, "something wrong happened")
			retval.put(('',''))


def print_to_screen(host, functions):
	str_to_print = timestamp + ';' + dip_hostname[host] + ';'
	for func in functions:
		try:
			f_name = func.__name__
			hostfunc = host + f_name
			if f_name == 'check_qps':
				str_to_print += str(dretval_get[hostfunc][0])
			elif f_name == 'check_cpu' or f_name == 'check_memory' or f_name == 'check_tcp_query' or f_name == 'check_recursion':
				str_to_print += dretval_get[hostfunc][0] + ';' + dretval_get[hostfunc][1] + ';'
			elif f_name == 'check_root':
				str_to_print += dretval_get[hostfunc][0] + dretval_get[hostfunc][1]
		except KeyError as e:
			write_to_file(dip_hostname[host], "print_to_screen", str(e))
		except:
			write_to_file(dip_hostname[host], "print_to_screen", "error printing certain hostfunc output")
	print str_to_print + dip_hostname[host]

	
class funcThread(threading.Thread):
	def __init__(self, host, func, retval, username, password):
		threading.Thread.__init__(self)
		self.host = host
		self.func = func
		self.retval = retval
		self.username = username
		self.password = password
	def run(self):
		self.func(self.host, self.retval, self.username, self.password)

def main():
	for host in hosts:
		f_name = 'ssh_connect'
		try:
			dclient[host] = paramiko.SSHClient()
			dclient[host].set_missing_host_key_policy(paramiko.AutoAddPolicy())
			dclient[host].connect(host, username = username, password = password, timeout = 10)
		except (ssh_exception.BadHostKeyException, ssh_exception.AuthenticationException, ssh_exception.SSHException, socket.error) as e:
			write_to_file(dip_hostname[host], f_name, str(e))
			ssh_connected[host] = 0
		except:
			write_to_file(dip_hostname[host], f_name, "something wrong happened")
			ssh_connected[host] = 0

	funcs = (check_qps, check_cpu, check_memory, check_recursion, check_tcp_query, check_root)

	for host in hosts:
		for func in funcs:
			hostfunc = host + func.__name__
			dretval[hostfunc] = Queue.Queue()
			dthread[hostfunc] = funcThread(host, func, dretval[hostfunc], username, password)
				
			dthread[hostfunc].start()
			if host == "10.113.176.17":
				lthread_jpr.append(dthread[hostfunc])
			else:
				lthread.append(dthread[hostfunc])
			
	for t in lthread:
		t.join()

	for t in lthread_jpr:
		t.join()
		
	for host in hosts:
		for func in funcs:
			try:
				f_name = func.__name__
				hostfunc = host + f_name
				dretval_get[hostfunc] = dretval[hostfunc].get_nowait()
			except Queue.Empty as e:
				write_to_file(dip_hostname[host], f_name, str(e))
			except:
				write_to_file(dip_hostname[host], f_name, "something wrong happened during data retrieval from certain thread")
				
	for host in hosts:
		dclient[host].close()
		print_to_screen(host, funcs)

if __name__ == '__main__':
	main()