#!/bin/bash
nf_conntrack_file="/proc/net/nf_conntrack"
ssh_command="ssh -q -l root"

function check_nf_conntrack () {
		nf_conntrack_lines=$($ssh_command $1 wc -l $nf_conntrack_file | awk '{print $1}')
		if [ $nf_conntrack_lines -eq 0 ];then
				echo "OK;nf_conntrack"
		else
				echo "NOK;nf_conntrack"
		fi
}

function check_cpu () {
        idle_cpu=$($ssh_command $1 top -b -n1 | grep Cpu | awk '{print $8}' | cut -d . -f1)
        if [[ $idle_cpu =~ ^[0-9]+$ ]] && cpu_usage=$((100 - $idle_cpu));then
				echo "$cpu_usage;cpu"
        else
				echo "idle_cpu is NaN"
        fi
}

function check_memory () {
        used_mem=$($ssh_command $1 free -m | grep -e \-\/\+ | awk '{print $3}')
        total_mem=$($ssh_command $1 free -m | grep "Mem:" | awk '{print $2}')
        if [[ $used_mem =~ ^[0-9]+$ ]] && [[ $total_mem =~ ^[0-9]+$ ]];then
                mem_usage=$(($used_mem * 100 / $total_mem))
						echo "$mem_usage;memory"
        else
                echo "used_mem and/or total_mem is NaN;memory"
        fi
}

function check_recursive () {
        recur_clients=$($ssh_command $1 rndc status | grep recursive | awk '{print $3}' | cut -d / -f1)
        max_recur_clients=$($ssh_command $1 rndc status | grep recursive | awk '{print $3}' | cut -d / -f3)

        if [[ $recur_clients =~ ^[0-9]+$ ]] && [[ $max_recur_clients =~ ^[0-9]+$ ]];then
                soft_limit=$(($max_recur_clients - 100))
                if [ $recur_clients -gt $soft_limit ];then
                        echo "recursive client : soft-limit exceeded;recursive"
                else
                        echo "$recur_clients;recursive"
                fi
        fi
}

function check_tcp () {
        tcp_clients=$($ssh_command $1 rndc status | grep tcp | awk '{print $3}' | cut -d / -f1)
        max_tcp_clients=$($ssh_command $1 rndc status | grep tcp | awk '{print $3}' | cut -d / -f2)
        soft_limit=10000
        if [[ $tcp_clients =~ ^[0-9]+$ ]] && [[ $max_tcp_clients =~ ^[0-9]+$ ]];then
                if [ $tcp_clients -gt $soft_limit ];then
                        echo "tcp client soft-limit exceeded;tcp"
                else
                        echo "$tcp_clients;tcp"
                fi
        fi
}

function check_root () {
	ls_root=(198.41.0.4 \
	192.228.79.201 \
	192.33.4.12 \
	199.7.91.13 \
	192.203.230.10 \
	192.5.5.241 \
	192.112.36.4 \
	198.97.190.53 \
	192.36.148.17 \
	192.58.128.30 \
	193.0.14.129 \
	199.7.83.42 \
	202.12.27.33)

	isOkay_root=0
	prob_root=()
	i=0
	j=0
	while [ $i -le 12 ];do
		root_status=$($ssh_command dig @${ls_root[$i]} com.|grep status |awk '{print $6}'| cut -c 1-7)
		if [[ $root_status != "NOERROR" ]];then
			prob_root[$j]=${ls_root[$i]}
			((j++))
			isOkay_root=1
		fi
		((i++))
	done
	if [ $isOkay_root -eq 0 ];then
		echo "OK;root_server"
	else
		echo "cannot resolve domain using following root: ${prob_root[@]};root_server"
	fi

function check_log () {
        err_count=$($ssh_command $1 cat /var/log/syslog | grep -i "$2" | wc -l)
        if [ $err_count -gt 30 ];then
                echo "many \"$2\" found in the log"
        else
                echo "error \"$2\" found : $err_count"
        fi
}

#ChangeMe
hosts="172.x.x.1 172.x.x.2 172.x.x.3"

for host in $hosts;do
		hostname=$($ssh_command $host hostname)
		echo "$hostname;`check_nf_conntrack $host`"
		echo "$hostname;`check_cpu $host`"
		echo "$hostname;`check_memory $host`"
		echo "$hostname;`check_recursive $host`"
		echo "$hostname;`check_tcp $host`"
		echo "$hostname;`check_root $host`"
		echo "$hostname;`check_log $host \"error sending response: unset\"`;check_log1"
		echo "$hostname;`check_log $host \"maximum number of FD events (64) received\"`;check_log2"
done
