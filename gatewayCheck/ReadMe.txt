Usage: ./gatewayCheck.sh

This script is run on DNS server or any other linux server whose link is not redundant, buat has option to reach multiple routers through switch.
The condition can be even worse when a server cannot be configured to set multiple default-gateway option.
This script was created to make round-robin switch from one gateway to another, if the main gateway failed to serve.