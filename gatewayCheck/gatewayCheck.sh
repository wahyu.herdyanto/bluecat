#!/bin/bash
#mailto: wahyu.herdyanto@dimensiondata.com
#last update: 21st July 2016
#
#if you want to add GATEWAY you have to:
#- define new Gateway IP Address
#- change value of certain variable and if-conditional, as marked (preceeded) by "CHANGE-ME" 
#
#*********************************************************************
# Set Variable
#*********************************************************************
#GATEWAYF5_1="10.x.x.1" # Gateway-1
#GATEWAYF5_2="10.x.x.2" # Gateway-2
GATEWAYF5_3="10.x.x.3" # Gateway-3
GATEWAYF5_4="10.x.x.4" # Gateway-4
LOG_FILE="/var/log/gateway.log" # logfile
LOG_TIME=`date +%b' '%d' '%T`

ping -c 2 $GATEWAYF5_3 > /dev/null 2>&1
PINGF5_3=$?

ping -c 2 $GATEWAYF5_4 > /dev/null 2>&1
PINGF5_4=$?

#GW_LST=($GATEWAYF5_1 $GATEWAYF5_2 $GATEWAYF5_3 $GATEWAYF5_4)
#PINGF5S=($PINGF5_1 $PINGF5_2 $PINGF5_3 $PINGF5_4)

#CHANGE-ME
TOTAL_GATEWAY=2
IDX_MAX=$(($TOTAL_GATEWAY - 1))
GW_LST=($GATEWAYF5_3 $GATEWAYF5_4)
PINGF5S=($PINGF5_3 $PINGF5_4)
#*********************************************************************

check_current_gw() {
#check current gateway which is currently used
	CURRENT_GW=`ip route show | grep default | awk '{ print $3 }'`
	ping -c 2 $CURRENT_GW > /dev/null 2>&1
	CURRENT_PING=$?
	
	if [[ $CURRENT_PING -eq 1 ]];then
		echo "$LOG_TIME:[NOK] Ping to Current Gateway : $CURRENT_GW" >> $LOG_FILE
	else
		echo "$LOG_TIME:[OK] Ping to Current Gateway : $CURRENT_GW" >> $LOG_FILE
	fi
}

change_gw() {
# change gw if current gateway is not reachable
	if [[ $CURRENT_PING -eq 1 ]];then
		i=0
		#search currentGW info
		while [[ ! $CURRENT_GW == ${GW_LST[$i]} ]];do
			((i++))
		done
				
		if [[ $i -ne $IDX_MAX ]];then
			j=$(($i + 1))
			while [[ $j -le $IDX_MAX ]];do
				if [[ ${PINGF5S[$j]} -ne 1 ]];then
					NEXT_GW=${GW_LST[$j]}
					break
				else
					((j++))
				fi
			done
			
		else
			j=0
			while [[ $j -le $i ]];do
				if [[ ${PINGF5S[$j]} -ne 1 ]];then
					NEXT_GW=${GW_LST[$j]}
					break
				else
					((j++))
				fi
			done
		fi

		ip route del default
		ip route add default via $NEXT_GW
		echo "$LOG_TIME:[NOK] Default Gateway CHANGED to $NEXT_GW" >> $LOG_FILE
	fi		
}

main_part() {
	check_current_gw
	change_gw
}

#CHANGE-ME if necessary 
if [[ $PINGF5_3 -eq "0" ]] || [[ $PINGF5_4 -eq "0" ]];then
	# if at least one of gateways reachable, start the loop
	main_part
	i=0
	while [[ $i -le $IDX_MAX ]];do
		if [[ ${PINGF5S[$i]} -ne 0 ]];then
			echo "$LOG_TIME:[NOK] Gateway ${GW_LST[$i]} is NOT reachable!" >> $LOG_FILE
		fi
		((i++))
	done
else
	# if gateways not reachable do not start the script
	echo "$LOG_TIME:[NOK] Warning, all defined gateways are not ok!" >> $LOG_FILE
fi
exit 0
